# Un Diccionario es una estructura de datos y un tipo de dato en Python con características
# especiales que nos permite almacenar cualquier tipo de valor como enteros, 
# cadenas, listas e incluso otras funciones.

dic = {
    
    "manzana": "roja",
    "banana": "amarilla",
    "pera": "verde"
}

#print(dir(dic))devuelve todos los metodos

print(dic)
print(dic["banana"])
print(dic.values())
print(dic.keys())


#la lista pueden tener vario tipos de variables
#funciona con subindices como los array

lista1 = [0, 5, 3, 6, -1, 10, 2]
lista2 = [9,11]
lista3 =lista1+lista2
#suma de listas
print(lista3)
#imprecion de rango con el operador dos puntos (:)
print(lista1[-3:])
#ordenamiento con sort
lista1.sort()
print(lista1)
#quitar elementos del subindice
lista1.pop(0)
print(lista1)
#remove: quita el elmento especifico

lista1.remove(10)
print(lista1)


#preguntar si exite un elemento
print(5 in lista1)